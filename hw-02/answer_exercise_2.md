kind: ReplicaSet
metadata:
  name: exc2
  labels:
    app: exc2
    tier: frontend
spec:
  replicas: 3
  selector:
    matchLabels:
      app: exc2
      tier: frontend
  template:
    metadata:
      labels:
        app: exc2
        tier: frontend
    spec:
      containers:
      - name: exc2
        image: nginx:1.19.4
        resources:
          requests:
            memory: "256Mi"
            cpu: "100m"
          limits:
            memory: "256Mi"
            cpu: "100m"
        ports:
        - containerPort: 80



-- ejecutar: kubectl create -f replicaset.yaml  
## ¿Cúal sería el comando que utilizarías para escalar el número de replicas a 10?
- kubectl scale --replicas=10 rs exc2

## Si necesito tener una replica en cada uno de los nodos de Kubernetes, ¿qué objeto se adaptaría mejor? (No es necesario adjuntar el objeto)
- DaemonSet