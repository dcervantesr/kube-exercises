apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: exc2
      tier: frontend
  template:
    metadata:
      labels:
        app: exc2
        tier: frontend
    spec:
      containers:
      - name: exc2
        image: nginx:1.19.4
        resources:
          requests:
            memory: "256Mi"
            cpu: "100m"
          limits:
            memory: "256Mi"
            cpu: "100m"
        ports:
        - containerPort: 80

## Despliega una nueva versión de tu nuevo servicio mediante la técnica “recreate”
Agrego dentro del spec del deploy la estrategia, que por defecto era RollingUpdate.
strategy:
    type: Recreate

## Despliega una nueva versión haciendo “rollout deployment”
kubectl set image deploy nginx-deployment exc2=nginx:1.19.5


## Realiza un rollback a la versión generada previamente
kubectl rollout undo deploy nginx-deployment --to-revision=1