apiVersion: v1
kind: Pod
metadata:
  name: nginx
  labels:
    app: nginx-server
spec:
  containers:
  - name: nginx
    image: nginx:1.19.4
    resources:
      requests:
        memory: "256Mi"
        cpu: "100m"
      limits:
        memory: "256Mi"
        cpu: "100m"
    ports:
    - containerPort: 80


-- ejecutar: kubectl create -f pods.yaml

## ¿Cómo puedo obtener las últimas 10 líneas de la salida estándar (logs generados por la aplicación)?
- kubectl logs -f --tail 10 nginx

## ¿Cómo podría obtener la IP interna del pod? Aporta capturas para indicar el proceso que seguirías.
- kubectl get pods -o wide
- imagenes en carpeta img_ex1

## ¿Qué comando utilizarías para entrar dentro del pod?
- kubectl exec --stdin --tty nginx -- /bin/bash

## Necesitas visualizar el contenido que expone NGINX, ¿qué acciones debes llevar a cabo?
- kubectl port-forward nginx 8080:80

## Indica la calidad de servicio (QoS) establecida en el pod que acabas de crear. ¿Qué lo has mirado?
- Guaranteed => la veo en la lista de info que pone al hacer: kubectl describe pods nginx