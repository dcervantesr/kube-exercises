## Exponiendo el servicio hacia el exterior (crea service1.yaml)
apiVersion: v1
kind: Service
metadata:
  name: service3
spec:
  selector:
    app: exc2
  ports:
    - protocol: TCP
      port: 80
      targetPort: 8080
  clusterIP: 10.0.171.239
  type: LoadBalancer
status:
  loadBalancer:
    ingress:
    - ip: 192.0.2.127

## De forma interna, sin acceso desde el exterior (crea service2.yaml)
apiVersion: v1
kind: Service
metadata:
  name: service1
spec:
  selector:
    app: exc2
  ports:
    - protocol: TCP
      port: 80
      targetPort: 9376


- ejecutar: kubectl create -f service2.yaml

## Abriendo un puerto especifico de la VM (crea service3.yaml)
apiVersion: v1
kind: Service
metadata:
  name: service3
spec:
  selector:
    app: exc2
  ports:
    - nodePort: 32766
      port: 80
      protocol: TCP
      targetPort: 8080
  type: NodePort

- ejecutar: kubectl create -f service3.yaml